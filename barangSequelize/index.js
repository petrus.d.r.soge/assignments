// Express
const express = require("express");
const app = express();

// Import routes
const transaksiRoutes = require("./routes/transaksiRoutes");
const barangRoutes = require("./routes/barangRoutes");
const pemasokRoutes = require("./routes/pemasokRoutes.js");

//Set body parser for HTTP post operation
app.use(express.json()); // support json encoded bodies
app.use(express.urlencoded({ extended: true })); // support encoded bodies

// set static assets to public directory (usually for images, videos, and other files)
app.use(express.static("public"));

// Import table relationship
require("./utils/associations");

app.use("/transaksi", transaksiRoutes);
app.use("/barang", barangRoutes);
app.use("/pemasok", pemasokRoutes);


// Server running
app.listen(3000, () => console.log("server running on port 3000"));
