const { transaksi, barang, pelanggan, pemasok } = require("../models");

class TransaksiController {
  async getAll(req, res) {
    try {
      let data = await transaksi.findAll({
        attributes: ["id", "jumlah", "total", ["createdAt", "waktu"]],
        include: [
          {
            model: barang,
            attributes: ["nama", "harga"],
            include: [{ model: pemasok, attributes: ["nama"] }],
          },
          {
            model: pelanggan,
            attributes: ["nama"],
          },
        ],
      });

      if (data.length === 0) {
        return res.status(404).json({
          message: "Transaksi Not Found",
        });
      }

      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }

  getOne(req, res) {
    transaksi
      .findOne({
        where: { id: req.params.id },
        attributes: ["id", "jumlah", "total", ["createdAt", "waktu"]],
        include: [
          {
            model: barang,
            attributes: ["nama", "harga"],
            include: [{ model: pemasok, attributes: ["nama"] }],
          },
          {
            model: pelanggan,
            attributes: ["nama"],
          },
        ],
      })
      .then((data) => {
        if (!data) {
          return res.status(404).json({
            message: "Transaksi Not Found",
          });
        }

        return res.status(200).json({
          message: "Success",
          data: data,
        });
      })
      .catch((e) => {
        return res.status(500).json({
          message: "Internal Server Error",
          error: e,
        });
      });
  }

  async create(req, res) {
    try {
      let createdData = await transaksi.create({
        id_barang: req.body.id_barang,
        id_pelanggan: req.body.id_pelanggan,
        jumlah: req.body.jumlah,
        total: req.body.total,
      });

      let data = await transaksi.findOne({
        where: { id: createdData.id },
        attributes: ["id", "jumlah", "total", ["createdAt", "waktu"]],
        include: [
          {
            model: barang,
            attributes: ["nama", "harga"],
            include: [{ model: pemasok, attributes: ["nama"] }],
          },
          {
            model: pelanggan,
            attributes: ["nama"],
          },
        ],
      });

      return res.status(201).json({
        message: "Success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }

  async update(req, res) {
    let update = {
      id_barang: req.body.id_barang,
      id_pelanggan: req.body.id_pelanggan,
      jumlah: req.body.jumlah,
      total: req.body.total,
    };

    try {
      let updatedData = await transaksi.update(update, {
        where: {
          id: req.params.id,
        },
      });

      let data = await transaksi.findOne({
        where: { id: req.params.id },
        attributes: ["id", "jumlah", "total", ["createdAt", "waktu"]],
        include: [
          {
            model: barang,
            attributes: ["nama", "harga"],
            include: [{ model: pemasok, attributes: ["nama"] }],
          },
          {
            model: pelanggan,
            attributes: ["nama"],
          },
        ],
      });

      return res.status(201).json({
        message: "Success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }

  async delete(req, res) {
    try {
      let data = await transaksi.destroy({ where: { id: req.params.id } });

      if (!data) {
        return res.status(404).json({
          message: "Transaksi Not Found",
        });
      }

      return res.status(200).json({
        message: "Success delete transaksi",
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
}

module.exports = new TransaksiController();
