const { ObjectId } = require("mongodb"); // Import ObjectId
const connection = require("../models");

class BarangController {
  // Get All Data
  async getAll(req, res) {
    const dbConnection = connection.db("penjualan_morning"); // Connect to database penjualan_morning
    const barang = dbConnection.collection("barang"); // Connect to table/collection barang
    try {
      let data = await barang.find({}).toArray(); // Get all data from barang table

      // If no data
      if (data.length === 0) {
        return res.status(404).json({
          message: "Not Found!",
        });
      }

      // If success
      return res.status(200).json({
        message: "Found Your Barang",
        data,
      });
    } catch (e) {
      // If failed
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
  async getOne(req, res) {
    try {
      const dbConnection = connection.db("penjualan_morning");
      const barang = dbConnection.collection("barang");
      let data = await barang.findOne({
        _id: new ObjectId(req.params.id),
      });

      // If Success
      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (e) {
      // if failed
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
  // create data
  async create(req, res) {
    try {
      const dbConnection = connection.db("penjualan_morning");
      const barang = dbConnection.collection("barang");
      // Insert data transaksi
      let data = await barang.insertOne({
        barang: req.body.barang,
        pemasok: req.body.pemasok,
        harga: req.body.harga,
      });

      // If success
      return res.status(200).json({
        message: "Successfully Created Barang!",
        data: data.ops[0],
      });
    } catch (e) {
      console.log(e);
      // If failed
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
  // Update data
  async update(req, res) {
    try {
      const dbConnection = connection.db("penjualan_morning");
      const barang = dbConnection.collection("barang");
      // Update data barang
      await barang.updateOne(
        {
          _id: new ObjectId(req.params.id),
        },
        {
          $set: {
            barang: req.body.barang,
            pemasok: req.body.pemasok,
            jumlah: req.body.jumlah,
          },
        }
      );

      // Find data that needs to be updated
      let data = await barang.findOne({
        _id: new ObjectId(req.params.id),
      });

      // If success
      return res.status(200).json({
        message: "Successfully Updated Your Barang!",
        data,
      });
    } catch (e) {
      // If failed
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
  // delete
  async delete(req, res) {
    try {
      const dbConnection = connection.db("penjualan_morning");
      const barang = dbConnection.collection("barang");
      // delete data depends on req.params.id
      let data = await barang.deleteOne({
        _id: new ObjectId(req.params.id),
      });

      // If success
      return res.status(200).json({
        message: "Successfully Delete Barang",
      });
    } catch (e) {
      // If failed
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
}

module.exports = new BarangController();
