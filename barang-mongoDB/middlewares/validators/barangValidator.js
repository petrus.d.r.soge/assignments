const validator = require("validator");
const { ObjectId } = require("mongodb");
const connection = require("../../models");
const penjualan = connection.db("penjualan_morning"); // Connect to penjualan database
const barang = penjualan.collection("barang"); // Connect to barang collection / table

exports.create = async (req, res, next) => {
  try {
    // Get barang and pemasok
    let findData = await Promise.all([
      penjualan.collection("barang").findOne({
        _id: new ObjectId(req.body.id_barang),
      }),
      penjualan.collection("pemasok").findOne({
        _id: new ObjectId(req.body.id_pemasok),
      }),
    ]);

    // Create errors variable
    let errors = [];

    // If nama not found
    if (!findData[0]) {
      errors.push("Barang Not Found");
    }

    // If pemasok not found
    if (!findData[1]) {
      errors.push("Pemasok Not Found");
    }

    if (!validator.isNumeric(req.body.harga)) {
      errors.push("Harga must be numbers");
    }

    // If errors length > 0, it will make errors message
    if (errors.length > 0) {
      // Because bad request
      return res.status(400).json({
        message: errors.join(", "),
      });
    }

    // add some req.body for used in Controller
    req.body.barang = findData[0];
    req.body.pemasok = findData[1];
    req.body.harga = findData[0];

    // It means that will be go to the next middleware
    next();
  } catch (e) {
    return res.status(500).json({
      message: "Internal Server Error",
      error: e,
    });
  }
};

exports.update = async (req, res, next) => {
  try {
    // Get barang and pemasok
    let findData = await Promise.all([
      penjualan.collection("barang").findOne({
        _id: new ObjectId(req.body.id_barang),
      }),
      penjualan.collection("pemasok").findOne({
        _id: new ObjectId(req.body.id_pemasok),
      }),
      barang.findOne({
        _id: new ObjectId(req.params.id),
      }),
    ]);

    // Create errors variable
    let errors = [];

    // If nama not found
    if (!findData[0]) {
      errors.push("Barang Not Found");
    }

    // If pemasok not found
    if (!findData[1]) {
      errors.push("Pemasok Not Found");
    }

    if (!findData[2]) {
      errors.push("Harga Not Found");
    }

    if (!validator.isNumeric(req.body.jumlah)) {
      errors.push("Harga must be a number");
    }

    // If errors length > 0, it will make errors message
    if (errors.length > 0) {
      // Because bad request
      return res.status(400).json({
        message: errors.join(", "),
      });
    }

    // add some req.body for used in Controller
    req.body.barang = findData[0];
    req.body.pemasok = findData[1];
    req.body.jumlah = findData[0];

    // It means that will be go to the next middleware
    next();
  } catch (e) {
    return res.status(500).json({
      message: "Internal Server Error",
      error: e,
    });
  }
};
