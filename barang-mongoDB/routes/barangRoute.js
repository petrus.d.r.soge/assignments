const express = require("express"); // Import express

// Import validator
const barangValidator = require("../middlewares/validators/barangValidator");

// Import controller
const barangController = require("../controllers/barangController");

// Make router
const router = express.Router();

// Get All Data
router.get("/", barangController.getAll);
router.get("/:id", barangController.getOne);
router.post("/", barangValidator.create, barangController.create);
router.put("/:id", barangValidator.update, barangController.update);
router.delete("/:id", barangController.delete);

// Export router
module.exports = router;
